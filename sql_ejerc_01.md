EJERCICIO 1 (19/11/2019)

1.- creo nueva base de datos y la selecciono:

mysql> CREATE DATABASE amazingCo;
Query OK, 1 row affected (0.01 sec)

mysql> USE amazingCo;
Database changed

2.- creo nueva tabla de clientes: Customers

mysql> CREATE TABLE Customers (
-> customersId INTEGER PRIMARY KEY AUTO_INCREMENT,
-> name TEXT,
-> lastName TEXT,
-> gender CHAR(1),
-> phone CHAR(15),
-> address TINYTEXT,
-> custom_type VARCHAR(1)
-> );
Query OK, 0 rows affected (0.02 sec)

3.- creo una nueva tabla de tipos de clientes: Customers_type

mysql> CREATE TABLE Customers_type (
-> typeId INTEGER PRIMARY KEY AUTO_INCREMENT,
-> name VARCHAR(20)
-> );
Query OK, 0 rows affected (0.02 sec)

mysql> SHOW TABLES;
+---------------------+
| Tables_in_amazingCo |
+---------------------+
| Customers |
| Customers_type |
+---------------------+
2 rows in set (0.00 sec)

4.- inserto datos en la tabla de tipos de clientes:

mysql> INSERT INTO Customers_type (name) VALUES ('Gold'), ('Silver'), ('Bronze');
Query OK, 3 rows affected (0.00 sec)
Records: 3 Duplicates: 0 Warnings: 0

mysql> SELECT \* FROM Customers_type;
+--------+--------+
| typeId | name |
+--------+--------+
| 1 | Gold |
| 2 | Silver |
| 3 | Bronze |
+--------+--------+
3 rows in set (0.00 sec)

5.- creo clientes en la tabla de clientes:

mysql> INSERT INTO Customers (name, lastName, gender, phone, address, custom_type) VALUES ('Ana', 'Velazquez', 'F', '(+33)148756932', 'Calle Mayor, 2, 3º. Madrid. Spain', '1');
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO Customers (name, lastName, gender, phone, address, custom_type) VALUES ('Jose', 'Ribera', 'M', '(+33)574369512', 'Rue Rivoli, 1, 5º. Paris. France', '2');
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO Customers (name, lastName, gender, phone, address, custom_type) VALUES ('Emilia', 'Urbino', 'F', '(+32)654258472', 'Main Street, 15, 1º. London. UK', '3');
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO Customers (name, lastName, gender, phone, address, custom_type) VALUES ('Arturo', 'Pinazo', 'M', '(+31)365784159', '5th Avenue, 2, 7º. New York. USA', '2');
Query OK, 1 row affected (0.00 sec)

mysql> INSERT INTO Customers (name, lastName, gender, phone, address, custom_type) VALUES ('Clara', 'Soler', 'F', '(+34)630888444', 'Other Street, 45, 10º. Toronto. Canada', '1');
Query OK, 1 row affected (0.00 sec)

mysql> SELECT \* FROM Customers;
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+
| customersId | name | lastName | gender | phone | address | custom_type |
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+
| 1 | Ana | Velazquez | F | (+33)148756932 | Calle Mayor, 2, 3º. Madrid. Spain | 1 |
| 2 | Jose | Ribera | M | (+33)574369512 | Rue Rivoli, 1, 5º. Paris. France | 2 |
| 3 | Emilia | Urbino | F | (+32)654258472 | Main Street, 15, 1º. London. UK | 3 |
| 4 | Arturo | Pinazo | M | (+31)365784159 | 5th Avenue, 2, 7º. New York. USA | 2 |
| 5 | Clara | Soler | F | (+34)630888444 | Other Street, 45, 10º. Toronto. Canada | 1 |
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+
5 rows in set (0.00 sec)

6.- Uno tablas mediante Inner JOIN mostrando el tipo de cliente que es cada uno:

mysql> SELECT \* FROM Customers INNER JOIN Customers_type ON Customers.custom_type = Customers_type.typeId;
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+--------+--------+
| customersId | name | lastName | gender | phone | address | custom_type | typeId | name |
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+--------+--------+
| 1 | Ana | Velazquez | F | (+33)148756932 | Calle Mayor, 2, 3º. Madrid. Spain | 1 | 1 | Gold |
| 2 | Jose | Ribera | M | (+33)574369512 | Rue Rivoli, 1, 5º. Paris. France | 2 | 2 | Silver |
| 3 | Emilia | Urbino | F | (+32)654258472 | Main Street, 15, 1º. London. UK | 3 | 3 | Bronze |
| 4 | Arturo | Pinazo | M | (+31)365784159 | 5th Avenue, 2, 7º. New York. USA | 2 | 2 | Silver |
| 5 | Clara | Soler | F | (+34)630888444 | Other Street, 45, 10º. Toronto. Canada | 1 | 1 | Gold |
+-------------+--------+-----------+--------+----------------+-----------------------------------------+-------------+--------+--------+
5 rows in set (0.00 sec)
